//
//  Football_AppUITests.swift
//  Football AppUITests
//
//  Created by Lewis Malin on 23/11/2016.
//  Copyright © 2016 Lewis Malin. All rights reserved.
//

import XCTest

class Football_AppUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        XCUIDevice.shared().orientation = .portrait
        
        let app = XCUIApplication()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLoadTeams() {
        let app = XCUIApplication()
        let cells = app.tables.cells
        app.buttons["leagueButton"].tap()
        app.pickerWheels.element.adjust(toPickerWheelValue: "Championship")
        sleep(2)
        XCTAssertEqual(cells.count, 24+2)
    }
    
    func testLoadTeamPage(){
        let app = XCUIApplication()
        let cells = app.tables.cells
        app.buttons["leagueButton"].tap()
        app.pickerWheels.element.adjust(toPickerWheelValue: "Championship")
        sleep(5)
        XCTAssertEqual(cells.count, 24+2)
        let team = cells.staticTexts["Newcastle United FC"]
        team.tap()
        let newcastle = app.staticTexts["Newcastle United FC"]
        XCTAssert(newcastle.exists)
    }
    
    func testClickFavourite(){
        let app = XCUIApplication()
        let cells = app.tables.cells
        app.buttons["leagueButton"].tap()
        app.pickerWheels.element.adjust(toPickerWheelValue: "Championship")
        sleep(5)
        XCTAssertEqual(cells.count, 24+2)
        let team = cells.staticTexts["Newcastle United FC"]
        team.tap()
        if app.buttons["favButton"].isSelected == false{
            app.buttons["favButton"].tap()}
        XCTAssertEqual(app.buttons["favButton"].isSelected, true)
    }

    
    func testStillFavourite(){
        let app = XCUIApplication()
        let cells = app.tables.cells
        app.buttons["leagueButton"].tap()
        app.pickerWheels.element.adjust(toPickerWheelValue: "Championship")
        sleep(5)
        XCTAssertEqual(cells.count, 24+2)
        let team = cells.staticTexts["Newcastle United FC"]
        team.tap()
        if app.buttons["favButton"].isSelected == false{
            app.buttons["favButton"].tap()}
        app.terminate()
        app.launch()
        app.buttons["leagueButton"].tap()
        app.pickerWheels.element.adjust(toPickerWheelValue: "Championship")
        sleep(2)
        XCTAssertEqual(cells.count, 24+2)
        team.tap()

        
        XCTAssertEqual(app.buttons["favButton"].isSelected, true)//will only work half the time, as favourite data persists
    }

    
}
