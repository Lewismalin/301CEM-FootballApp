//
//  PickerCell.swift
//  Football App
//
//  Created by Lewis Malin on 28/11/2016.
//  Copyright © 2016 Lewis Malin. All rights reserved.
//

import UIKit
import FootAppLibrary


protocol TablePopDelegate {
    func poptable(id: Int)
}




class PickerCell: UITableViewCell,UIPickerViewDataSource,UIPickerViewDelegate {
    @IBOutlet weak var LeaguePicker: UIPickerView!

    
    var delegate:TablePopDelegate? = nil
    
   override func awakeFromNib() {
        super.awakeFromNib()
        FootApp.sharedInstance.getLeague()
        self.LeaguePicker.dataSource = self
        self.LeaguePicker.delegate = self
            }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return FootApp.sharedInstance.leagueSearch.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return try? FootApp.sharedInstance.getLeagueName(atIndex: row)
        //return FootApp.sharedInstance.leagueSearch[row].name
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let del = self.delegate {
            del.poptable(id: row)
            
        }
    }
    
    
}
