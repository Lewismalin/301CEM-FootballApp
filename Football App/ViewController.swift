//
//  ViewController.swift
//  Football App
//
//  Created by Lewis Malin on 23/11/2016.
//  Copyright © 2016 Lewis Malin. All rights reserved.
//

import UIKit
import FootAppLibrary

class ViewController: UIViewController, UIWebViewDelegate {

    public var teamID:Int?
    
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var crestView: UIWebView!
    @IBOutlet weak var TeamName: UILabel!
    @IBOutlet weak var playedLabel: UILabel!
    @IBOutlet weak var drawsLabel: UILabel!
    @IBOutlet weak var lossesLabel: UILabel!
    @IBOutlet weak var gdLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var positionNum: UILabel!
    @IBOutlet weak var playedNum: UILabel!
    @IBOutlet weak var winsNum: UILabel!
    @IBOutlet weak var drawsNum: UILabel!
    @IBOutlet weak var lossNum: UILabel!
    @IBOutlet weak var gdNum: UILabel!
    @IBOutlet weak var pointsNum: UILabel!
    @IBOutlet weak var favButton: UIButton!
    

    @IBOutlet weak var winsLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let savedfave = UserDefaults.standard
        if let loadedItems: String = savedfave.object(forKey: "favourite") as? String{
            FootApp.sharedInstance.favourite = loadedItems
        }
        
        if let id:Int = self.teamID {            

            // acquiring and setting the text for the labels.
            favButton.setTitle(NSLocalizedString("Favourite", comment: "Favourite button"),for: .normal)
            let pos = NSLocalizedString("Position", comment: "league position")
            let played = NSLocalizedString("Played", comment: "amount of games played")
            let wins = NSLocalizedString("Wins", comment: "amount of games won")
            let draws = NSLocalizedString("Draws", comment: "amount of games drawn")
            let losses = NSLocalizedString("Losses", comment: "amount of games lost")
            let gd = NSLocalizedString("Goal Difference", comment: "a teams goal difference")
            let points = NSLocalizedString("Points", comment: "a teams total points")
            let position = try? String(FootApp.sharedInstance.getTeamPosition(atIndex: id))
            let play = try? String(FootApp.sharedInstance.getTeamPlayed(atIndex: id))
            let won = try? String(FootApp.sharedInstance.getTeamWins(atIndex: id))
            let drawn = try? String(FootApp.sharedInstance.getTeamDraws(atIndex: id))
            let lost = try? String(FootApp.sharedInstance.getTeamLosses(atIndex: id))
            let goald = try? String(FootApp.sharedInstance.getTeamGD(atIndex: id))
            let poi = try? String(FootApp.sharedInstance.getTeamPoints(atIndex: id))
            TeamName.text = try? FootApp.sharedInstance.getTeamName(atIndex: id)
            favButton.accessibilityLabel = "favButton"
            positionLabel.text = pos + ": "
            positionNum.text = position!
            playedLabel.text = played + ": " 
            playedNum.text = play!
            winsLabel.text = wins + ": "
            winsNum.text = won!
            drawsLabel.text = draws + ": "
            drawsNum.text = drawn!
            lossesLabel.text = losses + ": "
            lossNum.text = lost!
            gdLabel.text = gd + ": "
            gdNum.text = goald!
            pointsLabel.text = points + ": "
            pointsNum.text = poi!
            
            if FootApp.sharedInstance.favourite == TeamName.text{
                favButton.isSelected = true
            }
            
            //acquiring and loading the teams crest
            
            if let url = try? FootApp.sharedInstance.getTeamPic(atIndex: id){
                //loadCrest(teamurl: url)
                crestView.delegate = self
                
                if let urlrequest: NSURLRequest = NSURLRequest(url: URL(string: url)!){
                    self.crestView.scrollView.isScrollEnabled = false
                    self.crestView.scalesPageToFit = true
                    self.crestView.contentMode = .scaleToFill
                    self.crestView.loadRequest(urlrequest as URLRequest)
                    self.crestView.backgroundColor = UIColor.clear
                }
            }
            
        // Do any additional setup after loading the view, typically from a nib.
    }
    }

    
    @IBAction func faveButton(_ sender: UIButton) {
        if favButton.isSelected == true{
            favButton.isSelected = false
        }
        else{
            favButton.isSelected = true
            let fave  = try? FootApp.sharedInstance.getTeamName(atIndex: self.teamID!)
            FootApp.sharedInstance.addFavourite(fave: fave!)
            FootApp.sharedInstance.saveContext()
        }
    }
    
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



    //webview delegate
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let contentSize = self.crestView.scrollView.contentSize
        let webViewSize = self.crestView.bounds.size
        let scaleFactor = webViewSize.width / contentSize.width
        
        self.crestView.scrollView.minimumZoomScale = scaleFactor
        self.crestView.scrollView.maximumZoomScale = scaleFactor
        self.crestView.scrollView.zoomScale = scaleFactor
    }
}

