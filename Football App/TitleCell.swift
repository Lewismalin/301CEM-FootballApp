//
//  TitleCell.swift
//  Football App
//
//  Created by Lewis Malin on 28/11/2016.
//  Copyright © 2016 Lewis Malin. All rights reserved.
//

import UIKit
import FootAppLibrary


protocol CellDelegate {
    func cellupdate()
}

class TitleCell: UITableViewCell {

    
    var delegate:CellDelegate? = nil

    let selectTitle = NSLocalizedString("Select League", comment: "an instruction to ask to select a league")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        LeagueButton.setTitle(selectTitle, for: UIControlState.normal)
        // Initialization code
    }
    @IBOutlet weak var LeagueButton: UIButton!
    
    

    @IBAction func ClickButton(_ sender: UIButton) {
        if let del = self.delegate {
          del.cellupdate()
        }
        
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
