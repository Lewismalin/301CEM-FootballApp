//
//  LeagueSelectTableController.swift
//  Football App
//
//  Created by Lewis Malin on 28/11/2016.
//  Copyright © 2016 Lewis Malin. All rights reserved.
//

import UIKit
import FootAppLibrary

class LeagueSelectTableController: UITableViewController, CellDelegate, TablePopDelegate{
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

            
    

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {return 2}
        return FootApp.sharedInstance.searchData.count
    }
    
    
    
    
    var cellheight:Int = 0
    
    
    
     /**
     Changes the height for a cell with height "cellheight" when prompted.
     ```
     cellupdate()
     ```
     
     */
     
     
     
 
    func cellupdate() {
        if cellheight ==  0{
            cellheight = 100}
        else{
            cellheight = 0
        }
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = "LeagueCell"
        let id2 = "TitleCell"
        let id3 = "PickerCell"
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: id2, for: indexPath) as! TitleCell
                cell.delegate = self
                return cell

            }
            else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: id3, for: indexPath) as! PickerCell
                cell.delegate = self
                return cell

            }
        }
        

        if indexPath.section != 0{

                let cell = tableView.dequeueReusableCell(withIdentifier: id, for: indexPath)

            cell.tag = indexPath.row
                cell.textLabel?.text = try? FootApp.sharedInstance.getTeamName(atIndex: indexPath.row)

            return cell


            
        }
        
            let cell = tableView.dequeueReusableCell(withIdentifier: id, for: indexPath)
        
            return cell
        
    
    }
    /**
     Populates tableview cells with teams from a given league.
     ```
     poptable(id: 427)
     ```
     
     - Parameter id: The ID for a given league.
        */
    func poptable(id: Int) {
        let leagueid = try? FootApp.sharedInstance.getLeagueID(atIndex: id)
        //let leagueid = FootApp.sharedInstance.leagueSearch[id].id
        FootApp.sharedInstance.searchData.removeAll()
        
        DispatchQueue.main.async {
                    self.tableView.reloadData()
        }



        try? FootApp.sharedInstance.tablePop(withText: leagueid!, {
            DispatchQueue.main.async {
                self.tableView.reloadData()
                
            }
        })
        

    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if indexPath.row == 1{
                return CGFloat(cellheight)
            }
        }
        return 44
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            }

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    
    // Override to support editing the table view.
   /* override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */




    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTeam" {
            if let indexPath = self.tableView.indexPathForSelectedRow{
                if let navigationController = segue.destination as? UINavigationController{
                    if let teamcontroller = navigationController.topViewController as? ViewController{
                        teamcontroller.teamID = indexPath.row
                        
                    
                    }
                }
            }
        }
    }
    

}
