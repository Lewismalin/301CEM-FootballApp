//
//  FootAppLibrary.h
//  FootAppLibrary
//
//  Created by Lewis Malin on 09/11/2016.
//  Copyright © 2016 Lewis Malin. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for FootAppLibrary.
FOUNDATION_EXPORT double FootAppLibraryVersionNumber;

//! Project version string for FootAppLibrary.
FOUNDATION_EXPORT const unsigned char FootAppLibraryVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FootAppLibrary/PublicHeader.h>


